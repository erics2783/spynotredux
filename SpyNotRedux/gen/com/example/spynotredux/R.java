/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.spynotredux;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /** 
         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f080000;
        public static final int action_settings=0x7f08001e;
        public static final int and_version=0x7f08000e;
        public static final int andversion=0x7f08000f;
        public static final int apk=0x7f080002;
        public static final int applabel=0x7f080003;
        public static final int appname=0x7f080014;
        public static final int cat_count=0x7f08001b;
        public static final int cat_countVal=0x7f08001c;
        public static final int features=0x7f080008;
        public static final int insdate=0x7f080011;
        public static final int installed=0x7f080010;
        public static final int last_modify=0x7f080013;
        public static final int list=0x7f080001;
        public static final int menu_about=0x7f08001f;
        public static final int modified=0x7f080012;
        public static final int numPerms=0x7f080017;
        public static final int numPermsVal=0x7f080018;
        public static final int pack_name=0x7f080004;
        public static final int package_name=0x7f080005;
        public static final int path=0x7f08000d;
        public static final int path_info=0x7f08000c;
        public static final int perAveragVal=0x7f08001a;
        public static final int perAverage=0x7f080019;
        public static final int permissions=0x7f08000a;
        public static final int perms=0x7f08001d;
        public static final int req_feature=0x7f080009;
        public static final int req_permission=0x7f08000b;
        public static final int std_dev=0x7f080015;
        public static final int std_devVal=0x7f080016;
        public static final int version=0x7f080006;
        public static final int version_name=0x7f080007;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int app_detail=0x7f030001;
        public static final int applistview_style=0x7f030002;
        public static final int target_detail=0x7f030003;
    }
    public static final class menu {
        public static final int main=0x7f070000;
    }
    public static final class string {
        public static final int about_desc=0x7f050004;
        public static final int about_title=0x7f050003;
        public static final int action_about=0x7f050005;
        public static final int action_settings=0x7f050001;
        public static final int apk_title=0x7f050008;
        public static final int app_icon=0x7f050007;
        public static final int app_name=0x7f050000;
        public static final int appsize=0x7f05000f;
        public static final int cat_count=0x7f050018;
        public static final int features=0x7f05000b;
        public static final int frequency=0x7f050012;
        public static final int get_permissions=0x7f050002;
        public static final int installed=0x7f050010;
        public static final int modified=0x7f050011;
        public static final int numPerms=0x7f050016;
        public static final int package_name=0x7f050009;
        public static final int path_info=0x7f05000e;
        public static final int perAverage=0x7f050015;
        public static final int percentage=0x7f050013;
        public static final int permSTD=0x7f050014;
        public static final int permissions=0x7f05000c;
        public static final int req_sdk=0x7f05000d;
        public static final int std_dev=0x7f050017;
        public static final int title_activity_apk_list=0x7f050006;
        public static final int version=0x7f05000a;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
