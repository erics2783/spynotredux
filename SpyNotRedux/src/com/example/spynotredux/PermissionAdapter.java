package com.example.spynotredux;

import com.example.spynotredux.app.AppData;
import com.example.spynotredux.app.ApplicationDO;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
//import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
/*import android.widget.ImageView;
import android.widget.LinearLayout;*/
import android.widget.TextView;


public class PermissionAdapter extends Activity {

	private ApplicationDO currentApp;
	ListView listView;
    
    public PermissionAdapter(Activity context,int target_detail, ApplicationDO currentApp) {
        //super(context, R.layout.target_detail, currentApp);
    	super();
        this.currentApp = currentApp;
    }
    
    public int getCount() {
        return currentApp.perms.length;
    }
       
    
    public View getView(int position, View convertView, ViewGroup parent) {
    		View row = convertView;
        
        
        if(row == null) {
            LayoutInflater inflater = LayoutInflater.from(getBaseContext());
            row = inflater.inflate(R.layout.target_detail, null);            
        }
        
        //Application app = currentApp.get(position);
        
        
        if(currentApp != null) {
            TextView title = (TextView)row.findViewById(R.id.apk);
            TextView package_name = (TextView)row.findViewById(R.id.pack_name );
            TextView std_dev = (TextView)row.findViewById(R.id.std_dev);
            TextView numPerms = (TextView)row.findViewById(R.id.numPerms);
            TextView perAverage = (TextView)row.findViewById(R.id.perAverage);
            TextView cat_count = (TextView)row.findViewById(R.id.cat_count);
            
            listView = (ListView) findViewById(R.id.perms);
            listView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, currentApp.perms));
                       
            if(title != null) title.setTag(currentApp.getTitle());
            if(package_name != null) title.setTag(currentApp.getPackage_name());
            if(std_dev != null) title.setTag(currentApp.getSTD());
            if(numPerms != null) title.setTag(currentApp.getNumPerms());
            if(perAverage != null) title.setTag(currentApp.getPerAverage());
            if(cat_count != null) title.setTag(currentApp.getCat_count());
            
            
            
        }              
                            
        return row;
    
    }
	
	
}
