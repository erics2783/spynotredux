package com.example.spynotredux;


import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.spynotredux.app.AppData;
import com.example.spynotredux.app.ApplicationDO;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.util.Log;
 
public class AppInfo extends Activity {
 
    TextView title, package_name, std_dev, numPerms, perAverage, cat_count;
    //PackageInfo packageInfo;
    ApplicationDO currentApp;
    ListView listView;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.target_detail);
        
        this.findViewsById();
        
        AppData appData = (AppData) getApplicationContext();
        //packageInfo = appData.getPackageInfo();
        currentApp = appData.getApplicationDO();
 
        setValues();
        
  
    }
    
    
    private void findViewsById() {
    	title = (TextView) findViewById(R.id.applabel);
        package_name = (TextView) findViewById(R.id.package_name );
        std_dev = (TextView) findViewById(R.id.std_devVal);
        numPerms = (TextView) findViewById(R.id.numPermsVal);
        perAverage = (TextView)findViewById(R.id.perAveragVal);
        cat_count = (TextView)findViewById(R.id.cat_countVal);
        
        listView = (ListView) findViewById(R.id.perms);
        
                   
        /*if(title != null) title.setTag(currentApp.getTitle());
        if(package_name != null) title.setTag(currentApp.getPackage_name());
        if(std_dev != null) title.setTag(currentApp.getSTD());
        if(numPerms != null) title.setTag(currentApp.getNumPerms());
        if(perAverage != null) title.setTag(currentApp.getPerAverage());
        if(cat_count != null) title.setTag(currentApp.getCat_count());*/
    }
 
    private void setValues() {
    	title.setText(currentApp.title);
 
        // package name
    	package_name.setText(currentApp.package_name);
 
        // version name
    	std_dev.setText(currentApp.std_dev);
 
        // target version
    	numPerms.setText(currentApp.numPerms);
 
        // path
    	perAverage.setText(currentApp.perAverage);
 
        // first installation
    	cat_count.setText(currentApp.cat_count);

    	//listview 
    	try{
    		listView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, currentApp.perms));
    	}
    	catch(Exception e)
    	{
    		Log.d("---------------->error",e.toString());
    	}
    		
    }
 
       
 
}
 
    
    
 
