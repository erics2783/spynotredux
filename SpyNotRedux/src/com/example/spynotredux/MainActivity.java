package com.example.spynotredux;

import com.example.spynotredux.app.AppData;
import com.example.spynotredux.app.ApplicationDO;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
//import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.telephony.TelephonyManager;
import android.net.Uri;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;



public class MainActivity extends Activity
							implements OnItemClickListener{
	
	PackageManager pm = null;
/*	private List<ApplicationInfo> applist = null;
	private ApplicationAdapter listadaptor = null;*/
	
	ListView appList;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		pm = getPackageManager();
		//new LoadApplications().execute();
		List<PackageInfo> packageList = pm.getInstalledPackages(PackageManager.GET_PERMISSIONS);
		List<PackageInfo> packageList1 = new ArrayList<PackageInfo>();
		
		//To filter out System apps
        for(PackageInfo pi : packageList) {
            boolean b = isSystemPackage(pi);
            if(!b) {
                packageList1.add(pi);
            }
        }
		
     /*   Log.d("SpyNotRetarded", "#################################");
        Log.d("SpyNotRetarded", "#################################");
        Log.d("this is the package:",packageList.get(1).toString());
        Log.d("SpyNotRetarded", "#################################");*/
        
		appList = (ListView) findViewById(R.id.list);
		appList.setAdapter(new ApplicationAdapter(this, packageList1, pm));
		appList.setOnItemClickListener(this);
		
	}
	
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/** Called when the user clicks the Get Permissions button */
	@SuppressLint("NewApi") public void getPermissions(View view) {
		//PackageManager pm = getPackageManager();

		Log.d("SpyNotRedux", "#################################");
		Log.d("SpyNotRedux", "#################################");
		Log.d("SpyNotRedux", "#                               #");
		Log.d("SpyNotRedux", "#       --- SpyNotRedux ---     #");
		Log.d("SpyNotRedux", "#                               #");
		Log.d("SpyNotRedux", "#################################");
		Log.d("SpyNotRedux", "#################################");


			try {
				
				PackageInfo packageInfo = pm.getPackageInfo("android", PackageManager.GET_PERMISSIONS);
				//Get Permissions
				
				
				String[] requestedPermissions = packageInfo.requestedPermissions;
				if(requestedPermissions != null) {
					Log.d("string---> ", requestedPermissions.toString());
					String[] args = {"android", new JSONArray(Arrays.asList(requestedPermissions)).toString()};
					//new runPHP().execute(args);
				}
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
	}
	
	/*members to drive the list
	 * 
	 * 
	 * 
	 * 
	 * 
	 * */
	
	
/**
 * Settings dialog 
 */
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = true;

		switch (item.getItemId()) {
		case R.id.action_settings: {
			displayAboutDialog();

			break;
		}
		default: {
			result = super.onOptionsItemSelected(item);

			break;
		}
		}

		return result;
	}
/*
 * settings pop up 
 * displays a pop up 
 * this is not working like I want
 * ....toast dialog of selectedITem info*/
	private void displayAboutDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.about_title));
		builder.setMessage(getString(R.string.about_desc));


		builder.setNegativeButton("No Thanks!", new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int id) {
		            dialog.cancel();
		       }
		});

		builder.show();
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
	    return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
	            : false;}

	//@Override
	/*protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		//ApplicationInfo application = applist.get(position);
		PackageInfo packageinfo = (PackageInfo) l.getItemAtPosition(position);
		try {
			this junk launches the selected app from the list
			 * Intent intent = pm
					.getLaunchIntentForPackage(app.packageName);

			if (null != intent) {
				startActivity(intent);}
			
			AppData appData = (AppData) getApplicationContext();
			appData.setPackageInfo(packageinfo);

			Intent appInfo = new Intent(getApplicationContext(), ApkInfo.class);
			startActivity(appInfo);
			
			
			
			
			
			}
		  catch (ActivityNotFoundException e) {
			Toast.makeText(MainActivity.this, e.getMessage(),
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(MainActivity.this, e.getMessage(),
					Toast.LENGTH_LONG).show();
		}
	}*/
	
	@Override
	public void onItemClick(AdapterView<?> l, View v, int position, long id) {
		//super.onItemClick(l, v, position, id);

		final PackageInfo packageData = (PackageInfo) l.getItemAtPosition(position);

		
		String pTitle = "android";//pm.getApplicationLabel(packageData.applicationInfo).toString();
		String pName = "android";//packageData.applicationInfo.packageName;
		PackageManager pm2 = getPackageManager();
		
		PackageInfo pInfo;
		try {
			pInfo = pm2.getPackageInfo(pName, PackageManager.GET_PERMISSIONS);
		
		//Get Permissions
		String [] permList = pInfo.requestedPermissions;
		final runPHP myPhp = new runPHP();
		if (permList != null)
		{
			String[] args = {pTitle, pName, new JSONArray(Arrays.asList(permList)).toString()};
			myPhp.execute(args);
		}
		else
		{
			String[] nullPerms = new String[1];
			nullPerms[0] = "";
			String[] args = {pTitle, pName, new JSONArray(Arrays.asList(nullPerms)).toString()};
			myPhp.execute(args);
		}
		
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		  @Override
		  public void run() {
			  Log.d("AHAHAHAHAHAHA", "cat: " + myPhp.myApp.getCategory());
			  try {
				  
					AppData appData = (AppData) getApplicationContext();
					//appData.setPackageInfo(packageData);
					appData.setApplicationDO(myPhp.myApp);

					//Intent appInfo = new Intent(getApplicationContext(), ApkInfo.class);
					Intent appInfo = new Intent(getApplicationContext(), AppInfo.class);
					startActivity(appInfo);
				}
				  catch (ActivityNotFoundException e) {
					Toast.makeText(MainActivity.this, e.getMessage(),
							Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Log.d("EEEE", e.toString());
//					Toast.makeText(MainActivity.this, e.getMessage(),
//							Toast.LENGTH_LONG).show();
				}
		  }
		}, 10000);
        
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//		PackageInfo packageInfo = pm.getPackageInfo("com.example.spynotredux", PackageManager.GET_PERMISSIONS);
//		//Get Permissions
//		
//		
//		String[] requestedPermissions = packageInfo.requestedPermissions;
//		if(requestedPermissions != null) {
//			String[] args = {"android", new JSONArray(Arrays.asList(requestedPermissions)).toString()};
//			new runPHP().execute(args);
		
	}

	/*private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
		ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
		for (ApplicationInfo info : list) {
			try {
				if (null != pm.getLaunchIntentForPackage(info.packageName)) {
					applist.add(info);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return applist;
	}

	private class LoadApplications extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress = null;

		@Override
		protected Void doInBackground(Void... params) {
			applist = checkForLaunchIntent(pm.getInstalledApplications(PackageManager.GET_META_DATA));
			listadaptor = new ApplicationAdapter(MainActivity.this,
					R.layout.applistview_style, applist);

			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(Void result) {
			setListAdapter(listadaptor);
			progress.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(MainActivity.this, null,
					"Loading application info...");
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}*/
	
}

