package com.example.spynotredux;

import java.util.List;

public class App {
	public String app_name;
	public String app_category;
	public List<String> permissions;
	
	public App (String name, String cat, List<String> perms)
	{
		app_name = name;
		app_category = cat;
		permissions = perms;
	}
}
