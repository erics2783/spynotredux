package com.example.spynotredux.app;

import android.app.Application;
import android.content.pm.PackageInfo;


public class AppData extends Application{
	
	PackageInfo packageInfo;
	ApplicationDO currentApp;

	public PackageInfo getPackageInfo() {
		return packageInfo;
	}

	public void setPackageInfo(PackageInfo packageInfo) {
		this.packageInfo = packageInfo;
	}
	
	public ApplicationDO getApplicationDO(){
		return currentApp;
	}
	
	public void setApplicationDO(ApplicationDO currentApp){
		this.currentApp = currentApp;
	}

}
