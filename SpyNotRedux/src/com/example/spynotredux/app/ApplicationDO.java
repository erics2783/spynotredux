package com.example.spynotredux.app;

import java.util.ArrayList;


public class ApplicationDO {
	public String title;
	public String package_name;
	public String std_dev;
	public String numPerms;
	public String perAverage;
	public String cat_count;
	public String category;
  
    public String [] perms;
    
    public ApplicationDO(String title, String package_name, String cat, String std_dev, String numPerms, String perAverage, String cat_count, String[] perms)
    {
    	super();
    	this.title = title;
    	this.package_name = package_name;
    	this.std_dev = std_dev;
    	this.numPerms =numPerms;
    	this.perAverage = perAverage;
    	this.cat_count = cat_count;
    	this.perms = perms;
    	this.category = cat;
    }
    
       
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getPackage_name() {
        return package_name;
    }
    public void setPackage_name(String val) {
        this.package_name = val;
    }
    
    public String getSTD() {
        return std_dev;
    }
    public void setSTD(String val) {
        this.std_dev = val;
    }
    public String getNumPerms() {
        return numPerms;
    }
    public void setNumPerms(String val) {
        this.numPerms = val;
    }
    public String getPerAverage() {
        return perAverage;
    }
    public void setPerAverage(String val) {
        this.perAverage = val;
    }
    public String getCat_count() {
        return cat_count;
    }
    public void setCat_count(String val) {
        this.cat_count = val;
    }
    
    public String getCategory(){
    	return this.category;
    }
    
    public void setCategory(String val){
    	this.category = val;
    }
        
    
    
    class Permission{
    	String pName;
    	String pDesc;
    	String frequency;//number of apps with this permision
    	String percent;//percentage of apps with sex
    	
    	Permission(String pName,String pDesc,String frquency,String percent)
    	{
    		this.pName = pName;
    		this.pDesc = pDesc;
    		this.frequency = frquency;
    		this.percent =numPerms;
    	}
    }
}

