package com.example.spynotredux;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.example.spynotredux.app.ApplicationDO;


//import java.util.Iterator;

class runPHP extends AsyncTask<String, String, String> {
	private static String url_store_permissions = "http://68.63.211.243:8090/spynotredux.php";
	JSONParser jsonParser = new JSONParser();

	public ApplicationDO myApp;
	
	public runPHP()
	{
		myApp = new ApplicationDO("", "", "", "", "", "", "", new String[0]);
	}
	
//	public JSONObject getMyJSON()
//	{
//		//return myJSON;
//	}
	
	@Override
	protected String doInBackground(String... args){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("package_name", args[1]));
		params.add(new BasicNameValuePair("permissions", args[2]));
		JSONObject json = jsonParser.makeHttpRequest(url_store_permissions, "POST", params);
		Log.d ("JSON ---->", json.toString());
		args[1] = args[1].replace("[", "");
		args[1] = args[1].replace("]", "");
		String[] perms = args[1].split(",");		
		
		
		try { 
			int success =json.getInt("success");

			if (success == 1) {
				//successfully added permission
				
				
				String title = "";
				String category = "";
				String std = "";
				String avg = "";
				String permCount = "";
				String catCount = "";
				
				Log.d("-----?>", json.getString("category"));
				if (json.getString("category") == "No Category")
				{
					title = args[0];
					category = "No Category";
					std = "N/A";
					avg = "N/A";
					permCount = Integer.toString(perms.length);
					catCount = "N/A";
				}
				else
				{
					title = args[0];
					category = json.getString("category");
					std = Double.toString(json.getDouble("perm_count_std"));
					avg = Double.toString(json.getDouble("perm_count_avg"));
					permCount = Integer.toString(perms.length);
					try{
					catCount = Integer.toString(json.getInt("category_count"));}
					catch(org.json.JSONException je)
					{
						catCount = "N/A";
					}
				}
				
				Log.d("SpyNotRedux", "\n--------------------");
				Log.d("SpyNotRedux", args[0]);
				Log.d("SpyNotRedux", "Category: " +category);
				Log.d("SpyNotRedux", "# of Permissions Standard Deviation: " + std);
				Log.d("SpyNotRedux", "# of Permissions Average: " + avg);
				Log.d("SpyNotRedux", "# of Perms for this app: " + permCount);
				//Log.d("SpyNotRedux", "STD_DEVS for this app: " + fmt1.format((Math.abs(perms.length-json.getDouble("perm_count_avg"))/json.getDouble("perm_count_std"))));
				Log.d("SpyNotRedux", "Cat_count: " + catCount);
				
				myApp = new ApplicationDO(title, args[0], category, std, permCount, avg, catCount, perms);
				//PermissionAdapter adapter = new PermissionAdapter(this,R.layout.target_detail,currentApp);

				Log.d("CAPP", myApp.getCat_count());
				Log.d("CAPP", myApp.getCategory());
				Log.d("CAPP", myApp.getNumPerms());
				Log.d("CAPP", myApp.getPackage_name());
				Log.d("CAPP", myApp.getPerAverage());
				Log.d("CAPP", myApp.getSTD());
				Log.d("CAPP", myApp.getTitle());
				JSONArray perms_by_category = new JSONArray();
				Log.d("adafafaf", json.getString("category_perms"));
				try{
					perms_by_category = json.getJSONArray("category_perms");
				}
				catch(org.json.JSONException je)
				{
					perms_by_category = null;
				}
				
				if (perms_by_category != null){
					for(int i = 0; i < perms_by_category.length(); i++)
					{
						JSONObject curr_perm = (JSONObject) perms_by_category.get(i);
						Log.d("SpyNotRedux", " ");
						Log.d("SpyNotRedux", "-----------Permission Info-----------");
						Log.d("SpyNotRedux", "	Permission: " + curr_perm.getString("permission_name"));
						Log.d("SpyNotRedux", "	# of apps w/ permission: " + curr_perm.getString("num_that_have_this"));
						DecimalFormat fmt = new DecimalFormat("0");
						Log.d("SpyNotRedux", "	Percentage: " + fmt.format(curr_perm.getDouble("percentage") * 100) + "%");
	
					}
				}
				else{
					Log.d("spynotredux.php", "-----> Permissions failed to log");
					Log.d("spynotredux.php",json.getString("category_perms"));
				}
				// closing this screen
			} else {
				// failed to add permission
				Log.d("spynotredux.php", "-----> FAILURE");
				Log.d("spynotredux.php", json.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	
	protected void onPostExecute() 
	{
	}

}


 