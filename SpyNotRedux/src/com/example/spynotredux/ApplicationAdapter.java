/*package com.example.spynotredux;

import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

public class ApplicationAdapter extends ArrayAdapter<ApplicationInfo> {
	//private List<ApplicationInfo> appsList = null;
	private List<PackageInfo> packageList = null;
	private Context context;
	private PackageManager pm;*/

/*	public ApplicationAdapter(Context context, int textViewResourceId,
			List<ApplicationInfo> appsList) {
		super(context, textViewResourceId, appsList);
		this.context = context;
		this.appsList = appsList;
		pm = context.getPackageManager();
	}*/
	
	/*public ApplicationAdapter(Context context, int textViewResourceId,
			List<PackageInfo> packageList) {
		super();
		this.context = context;
		this.packageList = packageList;
		pm = context.getPackageManager();
	}*/

	/*@Override
	public int getCount() {
		return ((null != appsList) ? appsList.size() : 0);
	}

	@Override
	public ApplicationInfo getItem(int position) {
		return ((null != appsList) ? appsList.get(position) : null);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}*/

/*	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (null == view) {
			LayoutInflater layoutInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.applistview_style, null);
		}

		ApplicationInfo data = appsList.get(position);
		if (null != data) {
			TextView appName = (TextView) view.findViewById(R.id.app_name);
			TextView packageName = (TextView) view.findViewById(R.id.app_paackage);
			ImageView iconview = (ImageView) view.findViewById(R.id.app_icon);

			appName.setText(data.loadLabel(pm));
			packageName.setText(data.packageName);
			iconview.setImageDrawable(data.loadIcon(pm));
		}
		return view;
	}*/
	
	/*
	 * 
	 * 
	 */
/*
	private class ViewHolder {
		TextView apkName;
	}

	public int getCount() {
		return packageList.size();
	}

	public Object getItem(int position) {
		return packageList.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		LayoutInflater inflater = context.getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.apklist_item, null);
			holder = new ViewHolder();

			holder.apkName = (TextView) convertView.findViewById(R.id.appname);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		PackageInfo packageInfo = (PackageInfo) getItem(position);
		Drawable appIcon = packageManager
				.getApplicationIcon(packageInfo.applicationInfo);
		String appName = packageManager.getApplicationLabel(
				packageInfo.applicationInfo).toString();
		appIcon.setBounds(0, 0, 40, 40);
		holder.apkName.setCompoundDrawables(appIcon, null, null, null);
		holder.apkName.setCompoundDrawablePadding(15);
		holder.apkName.setText(appName);

		return convertView;
	}
};*/

/*junk
 * 
 * 
 * */


package com.example.spynotredux;

import com.example.spynotredux.R;
 
import java.util.List;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
 
public class ApplicationAdapter extends BaseAdapter {
 
    List<PackageInfo> packageList;
    Activity context;
    PackageManager packageManager;
 
    public ApplicationAdapter(Activity context, List<PackageInfo> packageList,
            PackageManager packageManager) {
        super();
        this.context = context;
        this.packageList = packageList;
        this.packageManager = packageManager;
    }
 
    private class ViewHolder {
        TextView apk_title;
    }
 
    public int getCount() {
        return packageList.size();
    }
 
    public Object getItem(int position) {
        return packageList.get(position);
    }
 
    public long getItemId(int position) {
        return 0;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = context.getLayoutInflater();
 
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.applistview_style, null);
            holder = new ViewHolder();
 
            holder.apk_title = (TextView) convertView.findViewById(R.id.appname);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
 
        PackageInfo packageInfo = (PackageInfo) getItem(position);
        Drawable appIcon = packageManager
                .getApplicationIcon(packageInfo.applicationInfo);
        String appName = packageManager.getApplicationLabel(
                packageInfo.applicationInfo).toString();
        appIcon.setBounds(0, 0, 40, 40);
        holder.apk_title.setCompoundDrawables(appIcon, null, null, null);
        holder.apk_title.setCompoundDrawablePadding(15);
        holder.apk_title.setText(appName);
 
        return convertView;
    }
}




